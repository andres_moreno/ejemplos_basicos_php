<?php

	function maximominimo($arNumeros)
	{
		$minimo=NULL;
		$maximo=NULL;
		
		foreach($arNumeros as $valor)
		{
			if($valor < $minimo or $minimo==NULL)
				$minimo=$valor;
			
			if($valor > $maximo or $maximo==NULL)
				$maximo=$valor;
		}
		
		return array('min'=>$minimo,'max'=>$maximo);
	}
	$datos=array(2,7,10,123,-34);
	$resultado=maximominimo($datos);
	
	print_r($datos);
	echo '<br />';
	echo 'El minimo del array es :',$resultado['min'];
	echo '<br />';
	echo 'El maximo del array es :',$resultado['max'];

?>
